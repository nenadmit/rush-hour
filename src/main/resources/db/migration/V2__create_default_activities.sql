INSERT INTO activities (duration,name,price)
values
       (20,'dental_checkup',10),
       (90,'dental_implant',350),
       (30,'tooth_extraction',50),
       (60,'emergency_dental_care',40),
       (45,'dental_bridge',30),
       (45,'root_canal',30),
       (120,'oral_surgery',500);
