create table activities
(
    id       integer          not null auto_increment,
    duration bigint           not null,
    name     varchar(255),
    price    double precision not null,
    primary key (id)
) engine = InnoDB;
create table appointment_activity
(
    appointment_id integer not null,
    activity_id    integer not null
) engine = InnoDB;
create table appointments
(
    id         integer not null auto_increment,
    end_date   timestamp,
    start_date timestamp,
    user_id    integer,
    primary key (id)
) engine = InnoDB;
create table roles
(
    id   integer not null auto_increment,
    name varchar(255),
    primary key (id)
) engine = InnoDB;
create table user_role
(
    user_id integer not null,
    role_id integer not null,
    primary key (user_id, role_id)
) engine = InnoDB;
create table users
(
    id         integer not null auto_increment,
    email      varchar(255),
    first_name varchar(255),
    last_name  varchar(255),
    password   varchar(255),
    primary key (id)
) engine = InnoDB;
alter table users
    add constraint UK_6dotkott2kjsp8vw4d0m25fb7 unique (email);
alter table appointment_activity
    add constraint FKyxk1m5dfbfs6xvxcb9bax5ba foreign key (activity_id) references activities (id);
alter table appointment_activity
    add constraint FKkf6h6m2l708m5uhxo3x80byo5 foreign key (appointment_id) references appointments (id);
alter table appointments
    add constraint FK886ced1atxgvnf1o3oxtj5m4s foreign key (user_id) references users (id);
alter table user_role
    add constraint FKt7e7djp752sqn6w22i6ocqy6q foreign key (role_id) references roles (id);
alter table user_role
    add constraint FKj345gk1bovqvfame88rcx7yyx foreign key (user_id) references users (id);

