create table appointments_reminder
(
    id   integer not null auto_increment,
    reminder_date timestamp,
    appointment_id integer,
    primary key (id)
) engine = InnoDB;
alter table appointments_reminder
    add constraint fk_appointment_reminder foreign key (appointment_id) references appointments (id);

