package com.internship.rush_hour.controller.user;

import com.internship.rush_hour.database.dto.UpdateUserDTO;
import com.internship.rush_hour.database.dto.UserDTO;
import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping(value = "/api/user")
public class UserProfileController {

  private UserService userService;

  @Autowired
  public UserProfileController(
      UserService userService) {
    this.userService = userService;
  }

  @GetMapping
  public ResponseEntity<UserDTO> getUser() {

    return ResponseEntity.ok(userService.getUserDTO(userService.getUserFromSecurityContext()));
  }

  @PutMapping
  public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UpdateUserDTO userDTO) {

    return ResponseEntity.ok(userService.update(userDTO,userService.getUserFromSecurityContext().getId()));
  }

  @DeleteMapping
  public ResponseEntity<String> deleteUser() {

    User user = userService.getUserFromSecurityContext();
    userService.delete(user.getId());

    return ResponseEntity.ok(String.format("User %s, successfully deleted", user.getEmail()));
  }
}
