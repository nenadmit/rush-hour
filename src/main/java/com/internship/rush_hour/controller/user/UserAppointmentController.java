package com.internship.rush_hour.controller.user;

import com.internship.rush_hour.controller.utility.QueryParameter;
import com.internship.rush_hour.database.dto.AppointmentDTO;
import com.internship.rush_hour.database.dto.UpdateAppointmentDTO;
import com.internship.rush_hour.database.entity.Activity;
import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.service.ActivityService;
import com.internship.rush_hour.database.service.AppointmentService;
import com.internship.rush_hour.database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/user/appointments")
public class UserAppointmentController {

  private UserService userService;
  private AppointmentService appointmentService;
  @Autowired
  private ActivityService activityService;

  @Autowired
  public UserAppointmentController(UserService userService, AppointmentService appointmentService) {
    this.userService = userService;
    this.appointmentService = appointmentService;
  }

  @GetMapping
  public ResponseEntity<List<AppointmentDTO>> getAllAppointments(QueryParameter queryParameter) {

    return ResponseEntity.ok(
        appointmentService.getAllByUser(
            userService.getUserFromSecurityContext().getId(), queryParameter));
  }

  @GetMapping(value = "/{appointmentId}")
  public ResponseEntity<AppointmentDTO> getSingleAppointment(@PathVariable int appointmentId) {

    return ResponseEntity.ok(
        appointmentService.findByIdAndUserId(
            appointmentId, userService.getUserFromSecurityContext().getId()));
  }

  @PostMapping
  public ResponseEntity<AppointmentDTO> createAppointment(
      @Valid @RequestBody AppointmentDTO appointmentDTO) {

    return ResponseEntity.ok(
        appointmentService.persist(appointmentDTO, userService.getUserFromSecurityContext()));
  }

  @PutMapping(value = "/{appointmentId}")
  public ResponseEntity<AppointmentDTO> updateAppointment(
      @PathVariable int appointmentId, @Valid @RequestBody UpdateAppointmentDTO appointmentDTO) {

    return ResponseEntity.ok(
        appointmentService.update(appointmentDTO, appointmentService.findById(appointmentId)));
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<String> deleteAppointment(@PathVariable int id) {

    User user = userService.getUserFromSecurityContext();
    appointmentService.delete(appointmentService.findByIdAndUserId(id, user.getId()).getId());

    return ResponseEntity.ok(String.format("Appointment with id %s, successfully deleted", id));
  }

}
