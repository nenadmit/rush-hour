package com.internship.rush_hour.controller.admin;

import com.internship.rush_hour.controller.utility.QueryParameter;
import com.internship.rush_hour.database.dto.CreateUserDTO;
import com.internship.rush_hour.database.dto.UpdateUserDTO;
import com.internship.rush_hour.database.dto.UserDTO;
import com.internship.rush_hour.database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("api/users")
public class AdminUsersController {

  private UserService userService;

  @Autowired
  public AdminUsersController(
      UserService userService) {
    this.userService = userService;
  }

  @GetMapping
  public ResponseEntity<List<UserDTO>> getAllUsers(QueryParameter queryParameter) {

    return ResponseEntity.ok(userService.findAll(queryParameter.getPageable()));
  }

  @GetMapping(value = "/{userId}")
  public ResponseEntity<UserDTO> getSingleUser(@PathVariable int userId) {

    return ResponseEntity.ok(userService.getUserDTO(userService.findById(userId)));
  }

  @PostMapping
  public ResponseEntity<UserDTO> createUser(@Valid @RequestBody CreateUserDTO userDTO) {

    return ResponseEntity.ok(userService.persist(userDTO));
  }

  @PutMapping(value = "/{userId}")
  public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UpdateUserDTO userDTO, @PathVariable int userId) {

    return ResponseEntity.ok(userService.update(userDTO,userId));
  }

  @DeleteMapping("/{userId}")
  public ResponseEntity<String> deleteUser(@PathVariable(value = "userId") int userId) {
    userService.delete(userId);
    return ResponseEntity.ok(String.format("User with id %s, successfully deleted.", userId));
  }
}
