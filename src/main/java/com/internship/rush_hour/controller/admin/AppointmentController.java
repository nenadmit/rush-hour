package com.internship.rush_hour.controller.admin;

import com.internship.rush_hour.controller.utility.QueryParameter;
import com.internship.rush_hour.database.dto.AppointmentDTO;
import com.internship.rush_hour.database.dto.UpdateAppointmentDTO;
import com.internship.rush_hour.database.service.ActivityService;
import com.internship.rush_hour.database.service.AppointmentService;
import com.internship.rush_hour.database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/appointments")
public class AppointmentController {

  private AppointmentService appointmentService;
  private ActivityService activityService;
  private UserService userService;

  @Autowired
  public AppointmentController(
      AppointmentService appointmentService,
      ActivityService activityService,
      UserService userService) {
    this.appointmentService = appointmentService;
    this.activityService = activityService;
    this.userService = userService;
  }

  @GetMapping
  public ResponseEntity<List<AppointmentDTO>> findAllAppointments(QueryParameter queryParameter) {

    return ResponseEntity.ok(appointmentService.getAll(queryParameter));
  }

  @GetMapping("/{id}")
  public ResponseEntity<AppointmentDTO> findSingleAppointment(@PathVariable int id) {

    return ResponseEntity.ok(appointmentService.getAppointmentDTO(appointmentService.findById(id)));
  }

  @GetMapping("/user/{userId}")
  public ResponseEntity<List<AppointmentDTO>> findAllAppointmentsByUser(
      @PathVariable int userId, QueryParameter queryParameter) {

    return ResponseEntity.ok(appointmentService.getAllByUser(userId, queryParameter));
  }

  @GetMapping("/user/{userId}/appointment/{id}")
  public ResponseEntity<AppointmentDTO> getSingleAppointmentByUser(
      @PathVariable int userId, @PathVariable int id) {
    return ResponseEntity.ok(appointmentService.findByIdAndUserId(id, userId));
  }

  @PostMapping(value = "/user/{userId}")
  public ResponseEntity<AppointmentDTO> createAppointment(
      @PathVariable int userId, @Valid @RequestBody AppointmentDTO appointmentDTO) {

    return ResponseEntity.ok(
        appointmentService.persist(appointmentDTO, userService.findById(userId)));
  }

  /**
   * TODO - CHECK THE MAPPING IF ITS RIGHT
   *
   * @param appointmentDTO
   * @return
   */
  @PutMapping(value = "/{appointmentId}")
  public ResponseEntity<AppointmentDTO> updateAppointment(
      @PathVariable int appointmentId, @Valid @RequestBody UpdateAppointmentDTO appointmentDTO) {

    return ResponseEntity.ok(
        appointmentService.update(appointmentDTO, appointmentService.findById(appointmentId)));
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<String> deleteAppointment(@PathVariable int id){

    appointmentService.delete(id);
    return ResponseEntity.ok(String.format("Appointment with id %s, successfully deleted"));
  }


  @GetMapping(value = "/activities/{activityId}")
  public ResponseEntity<List<AppointmentDTO>> getAppointmentsByActivity(
      @PathVariable int activityId, QueryParameter queryParameter) {

    return ResponseEntity.ok(
        appointmentService.findAllByActivity(
            activityService.getActivityEntity(activityService.findById(activityId)),
            queryParameter.getPageable()));
  }
}
