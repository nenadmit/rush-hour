package com.internship.rush_hour.controller.admin;

import com.internship.rush_hour.controller.utility.QueryParameter;
import com.internship.rush_hour.database.dto.ActivityDTO;
import com.internship.rush_hour.database.dto.AppointmentDTO;
import com.internship.rush_hour.database.service.ActivityService;
import com.internship.rush_hour.database.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/activities")
public class ActivityController {

  private ActivityService activityService;
  private AppointmentService appointmentService;

  @Autowired
  public ActivityController(
      ActivityService activityService, AppointmentService appointmentService) {
    this.activityService = activityService;
    this.appointmentService = appointmentService;
  }

  @GetMapping
  public ResponseEntity<List<ActivityDTO>> getAll(QueryParameter queryParameter) {

    return ResponseEntity.ok(activityService.findAll(queryParameter.getPageable()));
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<ActivityDTO> getSingle(@PathVariable int id) {

    return ResponseEntity.ok(activityService.findById(id));
  }

  @PostMapping
  public ResponseEntity<ActivityDTO> createActivity(@Valid @RequestBody ActivityDTO activityDTO) {

    return ResponseEntity.ok(activityService.persist(activityDTO));
  }

  @PutMapping
  public ResponseEntity<ActivityDTO> updateActivity(@Valid @RequestBody ActivityDTO activityDTO) {

    return ResponseEntity.ok(activityService.update(activityDTO));
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<String> deleteActivity(@PathVariable int id) {

    activityService.delete(activityService.findById(id).getId());

    return ResponseEntity.ok(String.format("Activity with id %s, successfully deleted.", id));
  }

  @GetMapping(value = "/{activityId}/appointments")
  public ResponseEntity<List<AppointmentDTO>> getAppointmentsByActivity(
      @PathVariable int activityId, QueryParameter queryParameter) {

    return ResponseEntity.ok(
        appointmentService.findAllByActivity(
            activityService.getActivityEntity(activityService.findById(activityId)),
            queryParameter.getPageable()));
  }


}
