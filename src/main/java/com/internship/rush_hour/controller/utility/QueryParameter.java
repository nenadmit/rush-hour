package com.internship.rush_hour.controller.utility;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.internship.rush_hour.exception_handler.exception.ParameterValidationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class QueryParameter {

  private int page = 1;
  private int size = 15;
  private String sortBy = "id";
  private String order = "asc";
  private Boolean inProgress;

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public String getSortBy() {
    return sortBy;
  }

  public void setSortBy(String sortBy) {
    this.sortBy = sortBy;
  }

  public Boolean isInProgress() {
    return inProgress;
  }

  public void setInProgress(Boolean inProgress) {
    this.inProgress = inProgress;
  }

  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
  }

  @JsonIgnore
  public Pageable getPageable() {
    try {

      return PageRequest.of(page - 1, size, getSort(order));
    } catch (IllegalArgumentException e) {
      throw new ParameterValidationException(e.getMessage());
    }
  }

  private Sort getSort(String order) {
    if (!(order.equalsIgnoreCase("desc")) && !(order.equalsIgnoreCase("asc"))) {
      throw new IllegalArgumentException(
          "Invalid order parameter, acceptable parameters are desc or asc");
    }
    return order.equalsIgnoreCase("asc")
        ? Sort.by(sortBy).ascending()
        : Sort.by(sortBy).descending();
  }
}
