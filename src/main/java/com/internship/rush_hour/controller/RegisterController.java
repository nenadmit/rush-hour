package com.internship.rush_hour.controller;

import com.internship.rush_hour.database.dto.CreateUserDTO;
import com.internship.rush_hour.database.dto.UserDTO;
import com.internship.rush_hour.database.service.UserService;
import com.internship.rush_hour.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping(SecurityConstants.REGISTRATION_URL)
public class RegisterController {


  private UserService userService;

  @Autowired
  public RegisterController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping
  public ResponseEntity<UserDTO> register(@Valid @RequestBody CreateUserDTO userDTO) {
    return ResponseEntity.ok(userService.persist(userDTO));
  }
}
