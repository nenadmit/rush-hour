package com.internship.rush_hour.controller;

import com.internship.rush_hour.controller.utility.QueryParameter;
import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.service.UserService;
import com.internship.rush_hour.exception_handler.exception.EntityNotFoundException;
import com.internship.rush_hour.exception_handler.exception.ParameterValidationException;
import com.internship.rush_hour.security.SecurityConstants;
import com.internship.rush_hour.security.jwt.JwtTokenUtility;
import com.internship.rush_hour.security.jwt.pojo.LoginRequest;
import com.internship.rush_hour.security.jwt.pojo.LoginResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(SecurityConstants.LOGIN_URL)
public class LoginController {

  private AuthenticationManager authenticationManager;
  private UserService userService;
  private JwtTokenUtility tokenUtility;

  public LoginController(
      AuthenticationManager authenticationManager,
      UserService userService,
      JwtTokenUtility tokenUtility) {
    this.authenticationManager = authenticationManager;
    this.userService = userService;
    this.tokenUtility = tokenUtility;
  }

  @PostMapping
  public ResponseEntity<LoginResponse> authenticate(@Valid @RequestBody LoginRequest request) {

    Optional<User> optionalUser = userService.findByEmail(request.getEmail());

    if (!optionalUser.isPresent()) {
      throw new EntityNotFoundException(
              String.format("User with email %s, not found.", request.getEmail()));
    }
    try {
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
    } catch (BadCredentialsException e) {
      throw new ParameterValidationException("Wrong password.");
    }

    return ResponseEntity.ok(new LoginResponse(tokenUtility.createToken(optionalUser.get())));
  }

}
