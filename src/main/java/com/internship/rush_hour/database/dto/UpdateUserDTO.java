package com.internship.rush_hour.database.dto;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;


public class UpdateUserDTO {

    private int id;
    @Size(max=30,message = "First Name value must be less than 30 characters.")
    private String firstName;
    @Size(max=30,message = "First Name value must be less than 30 characters.")
    private String lastName;
    @Email
    private String email;
    @Size(min = 6,message = "Password must be at least 6 characters")
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
