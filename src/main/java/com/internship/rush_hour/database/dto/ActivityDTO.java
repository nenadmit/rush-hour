package com.internship.rush_hour.database.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ActivityDTO {

    private int id;
    @NotBlank
    @Size(min = 3,max = 35,message = "Activity name can't be shorter that 3 characters or longer than 35 characters")
    private String name;
    @Min(value = 5,message = "Duration can't be less than 5  minutes")
    private long duration;
    @Min(value = 1,message = "Price cannot be less than 1")
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
