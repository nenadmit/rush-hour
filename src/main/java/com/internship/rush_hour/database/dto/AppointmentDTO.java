package com.internship.rush_hour.database.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.internship.rush_hour.database.CustomDateDeserializer;
import com.internship.rush_hour.database.CustomDateSerializer;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class AppointmentDTO {

  private int id;

  @Future(message = "Date must reference a future date")
  @NotNull(message = "startDate cannot be blank")
  @JsonDeserialize(using = CustomDateDeserializer.class)
  @JsonSerialize(using = CustomDateSerializer.class)
  private Date startDate;

  @JsonDeserialize(using = CustomDateDeserializer.class)
  @JsonSerialize(using = CustomDateSerializer.class)
  private Date endDate;
  private Set<ActivityDTO> activities = new HashSet<>();

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Set<ActivityDTO> getActivities() {
    return activities;
  }
  public void setActivities(Set<ActivityDTO> activities) {
    this.activities = activities;
  }



}
