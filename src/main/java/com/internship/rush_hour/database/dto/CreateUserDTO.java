package com.internship.rush_hour.database.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;



public class CreateUserDTO {

    @Size(max=30,message = "First Name value must be less than 30 characters.")
    @NotBlank(message = "First Name cannot be blank")
    private String firstName;
    @Size(max=30,message = "First Name value must be less than 30 characters.")
    @NotBlank(message = "First Name cannot be blank")
    private String lastName;
    @NotBlank
    @Email
    private String email;
    @NotBlank
    @Size(min=6,message = "Password can't be less than 6 characters.")
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
