package com.internship.rush_hour.database;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateSerializer extends JsonSerializer<Date> {

  private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

  @Override
  public void serialize(Date value, JsonGenerator generator, SerializerProvider serializer)
      throws IOException {
    if (value == null) {
      generator.writeNull();
    } else {
      generator.writeString(formatter.format(value.getTime()));
    }
  }
}
