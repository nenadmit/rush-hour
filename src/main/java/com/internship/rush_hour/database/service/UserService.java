package com.internship.rush_hour.database.service;

import com.internship.rush_hour.database.dto.CreateUserDTO;
import com.internship.rush_hour.database.dto.UpdateUserDTO;
import com.internship.rush_hour.database.dto.UserDTO;
import com.internship.rush_hour.database.entity.Role;
import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.repository.RoleRepository;
import com.internship.rush_hour.database.repository.UserRepository;
import com.internship.rush_hour.events.mail_events.SuccessfulRegistrationEvent;
import com.internship.rush_hour.exception_handler.exception.EntityAlreadyPresentException;
import com.internship.rush_hour.exception_handler.exception.EntityNotFoundException;
import com.internship.rush_hour.exception_handler.exception.ParameterValidationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.internship.rush_hour.security.SecurityConstants.*;

@Service
public class UserService {

  private UserRepository userRepository;
  private RoleRepository roleRepository;
  private ModelMapper modelMapper;
  private PasswordEncoder passwordEncoder;
  private ApplicationEventPublisher eventPublisher;


  @Autowired
  public UserService(
      UserRepository userRepository,
      RoleRepository roleRepository,
      ModelMapper modelMapper,
      PasswordEncoder passwordEncoder,
      ApplicationEventPublisher eventPublisher) {
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
    this.modelMapper = modelMapper;
    this.passwordEncoder = passwordEncoder;
    this.eventPublisher = eventPublisher;
  }

  @Transactional
  public UserDTO persist(CreateUserDTO userDTO) {
    User user = getUserEntity(userDTO);

    this.prepareForPersist(user,userDTO.getEmail());
    encodePassword(user);
    userRepository.save(user);
    eventPublisher.publishEvent(
        new SuccessfulRegistrationEvent(this, user.getEmail(), user.getFirstName()));

    return getUserDTO(user);
  }

  @Transactional
  public User prepareForPersist(User user,String email){

    if (user.getRoles().isEmpty()) {
      setRole(user, ROLE_USER);
    }

    Optional<User> optionalUser = findByEmail(email);
    if (optionalUser.isPresent()) {
      throw new EntityAlreadyPresentException(
              String.format("User with email %s, already present.", email));
    }
    return user;
  }

  @Transactional
  public UserDTO update(UpdateUserDTO newUserDto, int id) {

    User oldUser = findById(id);

    if (newUserDto.getFirstName() != null) {
      oldUser.setFirstName(newUserDto.getFirstName());
    }
    if (newUserDto.getLastName() != null) {
      oldUser.setLastName(newUserDto.getLastName());
    }
    if (newUserDto.getEmail() != null && !(newUserDto.getEmail().equals(oldUser.getEmail()))) {
      if (!(findByEmail(newUserDto.getEmail()).isPresent())) {
        oldUser.setEmail(newUserDto.getEmail());
      } else {
        throw new EntityAlreadyPresentException(
            String.format("User with an email %s, already present", newUserDto.getEmail()));
      }
    }
    if (newUserDto.getPassword() != null) {
      oldUser.setPassword(newUserDto.getPassword());
      encodePassword(oldUser);
    }

    return getUserDTO(userRepository.save(oldUser));
  }

  @Transactional
  public void delete(int id) {

    findById(id);
    userRepository.deleteById(id);
  }

  @Transactional
  public List<UserDTO> findAll(Pageable pageable) {

    Page<User> users;
    try {
      users = userRepository.findAll(pageable);
    } catch (PropertyReferenceException e) {
      throw new ParameterValidationException(e.getMessage());
    }
    return users.stream().map(this::getUserDTO).collect(Collectors.toList());
  }

  @Transactional
  public User findById(int id) {

    Optional<User> optionalUser = userRepository.findById(id);
    if (!optionalUser.isPresent()) {
      throw new EntityNotFoundException(String.format("User with id %s, not found", id));
    }
    return optionalUser.get();
  }

  @Transactional
  public Optional<User> findByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  public User getUserFromSecurityContext() {
    UserDetails userDetails =
        (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    Optional<User> optionalUser = userRepository.findByEmail(userDetails.getUsername());

    return optionalUser.orElse(null);
  }

  public UserDTO getUserDTO(User user) {
    return modelMapper.map(user, UserDTO.class);
  }

  private User getUserEntity(CreateUserDTO userDTO) {
    return modelMapper.map(userDTO, User.class);
  }

  private void encodePassword(User user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));
  }

  private User setRole(User user, String role) {
    Optional<Role> optionalRole = roleRepository.findByName(ROLE_PREFIX + role);
    if (optionalRole.isPresent() && !user.getRoles().contains(optionalRole.get())) {
      user.getRoles().add(optionalRole.get());
    }
    return user;
  }
}
