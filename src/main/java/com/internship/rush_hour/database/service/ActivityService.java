package com.internship.rush_hour.database.service;

import com.internship.rush_hour.database.dto.ActivityDTO;
import com.internship.rush_hour.database.entity.Activity;
import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.database.repository.ActivityRepository;
import com.internship.rush_hour.database.repository.AppointmentRepository;
import com.internship.rush_hour.exception_handler.exception.EntityNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ActivityService {

  private ActivityRepository repository;
  @Autowired
  private AppointmentRepository appointmentRepository;
  private ModelMapper modelMapper;

  @Autowired
  public ActivityService(ActivityRepository repository, ModelMapper modelMapper) {
    this.repository = repository;
    this.modelMapper = modelMapper;
  }

  public ActivityDTO persist(ActivityDTO activityDTO) {

    return getActivityDTO(repository.save(getActivityEntity(activityDTO)));
  }

  public ActivityDTO update(ActivityDTO activityDTO) {
    if (!repository.findById(activityDTO.getId()).isPresent()) {
      throw new EntityNotFoundException(
          String.format("Activity with an id %s, not found.", activityDTO.getId()));
    }
    return getActivityDTO(repository.save(getActivityEntity(activityDTO)));
  }

  public ActivityDTO findById(int id) {

    Optional<Activity> optionalActivity = repository.findById(id);
    if (!optionalActivity.isPresent()) {
      throw new EntityNotFoundException(String.format("Activity with id %s, not found.", id));
    }
    return getActivityDTO(optionalActivity.get());
  }

  public void delete(int id) {
    Optional<Activity> optionalActivity = repository.findById(id);
    if(!optionalActivity.isPresent()){
      throw new EntityNotFoundException(
              String.format("Activity with an id %s, not found.", id));
    }
    Activity activity = optionalActivity.get();
    Set<Appointment> appointments = appointmentRepository.findAllByActivities(activity);
    appointments.stream().filter(appointment -> appointment.getActivities().contains(activity))
            .forEach(activityList -> activityList.getActivities().remove(activity));

    System.out.println(appointments + " AFTER DELETING");

    repository.deleteById(id);
  }

  public List<ActivityDTO> findAll(Pageable pageable) {
    return repository.findAll(pageable).stream()
        .map(this::getActivityDTO)
        .collect(Collectors.toList());
  }

  public Page<Activity> findAllByAppointment(Appointment appointment, Pageable pageable) {
    return repository.findAllByAppointments(appointment, pageable);
  }

  public ActivityDTO getActivityDTO(Activity activity) {
    return modelMapper.map(activity, ActivityDTO.class);
  }

  public Activity getActivityEntity(ActivityDTO activityDTO) {
    return modelMapper.map(activityDTO, Activity.class);
  }
}
