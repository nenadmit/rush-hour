package com.internship.rush_hour.database.service;

import com.internship.rush_hour.database.entity.AppointmentReminder;
import com.internship.rush_hour.database.repository.AppointmentReminderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ReminderService {

    private AppointmentReminderRepository reminderRepository;

    @Autowired
    public ReminderService(AppointmentReminderRepository reminderRepository) {
        this.reminderRepository = reminderRepository;
    }

    public List<AppointmentReminder> findAllBetweenTwoDates(Date startDate, Date endDate){
        return reminderRepository.findAllBetweenTwoDates(startDate,endDate);
    }

    public void deleteReminder(AppointmentReminder reminder){
        reminderRepository.delete(reminder);
    }
}
