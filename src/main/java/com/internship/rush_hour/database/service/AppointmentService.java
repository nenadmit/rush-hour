package com.internship.rush_hour.database.service;

import com.internship.rush_hour.controller.utility.QueryParameter;
import com.internship.rush_hour.database.dto.ActivityDTO;
import com.internship.rush_hour.database.dto.AppointmentDTO;
import com.internship.rush_hour.database.dto.UpdateAppointmentDTO;
import com.internship.rush_hour.database.entity.Activity;
import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.database.entity.AppointmentReminder;
import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.repository.ActivityRepository;
import com.internship.rush_hour.database.repository.AppointmentRepository;
import com.internship.rush_hour.events.mail_events.AppointmentScheduledEvent;
import com.internship.rush_hour.exception_handler.exception.EntityAlreadyPresentException;
import com.internship.rush_hour.exception_handler.exception.EntityNotFoundException;
import com.internship.rush_hour.exception_handler.exception.ParameterValidationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AppointmentService {

  private AppointmentRepository repository;
  private ActivityRepository activityRepository;
  private ModelMapper modelMapper;
  private ApplicationEventPublisher eventPublisher;

  @Autowired
  public AppointmentService(
      AppointmentRepository repository,
      ActivityRepository activityRepository,
      ModelMapper modelMapper,
      ApplicationEventPublisher eventPublisher) {
    this.repository = repository;
    this.activityRepository = activityRepository;
    this.modelMapper = modelMapper;
    this.eventPublisher = eventPublisher;
  }

  @Transactional
  public AppointmentDTO persist(AppointmentDTO appointmentDTO, User user) {

    Appointment appointment = prepareForPersist(getAppointmentEntity(appointmentDTO), user);
    repository.save(appointment);
    eventPublisher.publishEvent(new AppointmentScheduledEvent(this, appointment));

    return getAppointmentDTO(appointment);
  }

  public Appointment prepareForPersist(Appointment appointment, User user) {

    if (appointment.getActivities().isEmpty()) {
      throw new ParameterValidationException("Activities cannot be empty");
    }
    appointment.calculateEndDate();
    this.checkForDateOverlap(appointment);
    appointment.setUser(user);
    this.createAppointmentReminder(appointment);

    return appointment;
  }

  @Transactional
  public AppointmentDTO update(UpdateAppointmentDTO appointmentDTO, Appointment oldAppointment) {

    this.verifyAppointmentActivities(appointmentDTO.getActivities());
    Appointment newAppointment = getAppointmentEntity(appointmentDTO);
    newAppointment.setId(oldAppointment.getId());
    newAppointment.calculateEndDate();
    checkForDateOverlap(newAppointment);
    newAppointment.setUser(oldAppointment.getUser());
    this.createAppointmentReminder(newAppointment);

    return getAppointmentDTO(repository.save(newAppointment));
  }

  public List<AppointmentDTO> getAll(QueryParameter queryParameter) {
    return getAppointmentByProgress(queryParameter).stream()
        .map(this::getAppointmentDTO)
        .collect(Collectors.toList());
  }

  public List<AppointmentDTO> getAllByUser(int userId, QueryParameter queryParameter) {
    return getAppointmentByProgressUser(userId, queryParameter).stream()
        .map(this::getAppointmentDTO)
        .collect(Collectors.toList());
  }

  @Transactional
  public Appointment findById(int id) {

    Optional<Appointment> optionalAppointment = repository.findById(id);
    if (!optionalAppointment.isPresent()) {
      throw new EntityNotFoundException(String.format("Appointment with id %s, not found", id));
    }
    return optionalAppointment.get();
  }

  @Transactional
  public AppointmentDTO findByIdAndUserId(int id, int userId) {

    Optional<Appointment> optionalAppointment = repository.findByIdAndUserId(id, userId);
    if (!optionalAppointment.isPresent()) {
      throw new EntityNotFoundException(
          String.format("Appointment with id %s, not found for user %s,", id, userId));
    }
    return getAppointmentDTO(optionalAppointment.get());
  }

  public void delete(int id) {

    Optional<Appointment> optionalAppointment = repository.findById(id);
    if (!optionalAppointment.isPresent()) {
      throw new EntityNotFoundException(String.format("Appointment with id %s, not found", id));
    }
    repository.delete(optionalAppointment.get());
  }

  public void checkForDateOverlap(Appointment appointment) {

    List<Appointment> appointments =
        repository.findOverlappingAppointments(
            appointment.getStartDate(), appointment.getEndDate());

    if (appointments.size() >= 1 && !appointments.get(0).equals(appointment)) {
      throw new EntityAlreadyPresentException(
          String.format("%s date already taken by other appointment", appointment.getStartDate()));
    }
  }

  public List<AppointmentDTO> findAllByActivity(Activity activity, Pageable pageable) {

    return repository.findAllByActivities(activity, pageable).stream()
        .map(this::getAppointmentDTO)
        .collect(Collectors.toList());
  }

  public AppointmentDTO getAppointmentDTO(Appointment appointment) {
    return modelMapper.map(appointment, AppointmentDTO.class);
  }

  private Appointment getAppointmentEntity(AppointmentDTO appointmentDTO) {
    return modelMapper.map(appointmentDTO, Appointment.class);
  }

  public Appointment getAppointmentEntity(UpdateAppointmentDTO appointmentDTO) {
    return modelMapper.map(appointmentDTO, Appointment.class);
  }

  private Page<Appointment> getAppointmentByProgress(QueryParameter queryParameter) {
    if (queryParameter.isInProgress() == null) {
      return findAll(queryParameter.getPageable());
    }
    return queryParameter.isInProgress().booleanValue()
        ? findAllInProgress(queryParameter.getPageable())
        : findAllFinished(queryParameter.getPageable());
  }

  private Page<Appointment> getAppointmentByProgressUser(
      int userId, QueryParameter queryParameter) {
    if (queryParameter.isInProgress() == null) {
      return findByUserId(userId, queryParameter.getPageable());
    }
    return queryParameter.isInProgress().booleanValue()
        ? findAllInProgressByUserId(userId, queryParameter.getPageable())
        : findAllFinishedByUserId(userId, queryParameter.getPageable());
  }

  private Page<Appointment> findAll(Pageable pageable) {
    return repository.findAll(pageable);
  }

  private Page<Appointment> findAllInProgress(Pageable pageable) {
    return repository.findAllByStartDateAfter(new Date(), pageable);
  }

  private Page<Appointment> findAllFinished(Pageable pageable) {
    return repository.findAllByStartDateBefore(new Date(), pageable);
  }

  private Page<Appointment> findAllInProgressByUserId(int userId, Pageable pageable) {
    return repository.findAllByUserIdAndStartDateAfter(userId, new Date(), pageable);
  }

  private Page<Appointment> findAllFinishedByUserId(int userId, Pageable pageable) {
    return repository.findAllByUserIdAndStartDateBefore(userId, new Date(), pageable);
  }

  private Page<Appointment> findByUserId(int id, Pageable pageable) {
    return repository.findAllByUserId(id, pageable);
  }

  private void createAppointmentReminder(Appointment appointment) {
    AppointmentReminder reminder = new AppointmentReminder();
    reminder.setAppointment(appointment);
    reminder.setReminderDate(new Date(appointment.getStartDate().getTime() - 1_800_000));
    appointment.setReminder(reminder);
  }

  private void verifyAppointmentActivities(Set<ActivityDTO> activities) {

    for (ActivityDTO activity : activities) {
      if (!activityRepository.findById(activity.getId()).isPresent()) {
        throw new EntityNotFoundException(
            String.format("Activity with id %s, not found", activity.getId()));
      }
    }
  }
}
