package com.internship.rush_hour.database.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "appointments")
public class Appointment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm",timezone = "Europe/Madrid")
  private Date startDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm",timezone = "Europe/Madrid")
  private Date endDate;

  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.PERSIST})
  @JoinColumn(name = "user_id")
  @JsonIgnore
  private User user;

  @ManyToMany(cascade = {CascadeType.MERGE,CascadeType.DETACH}, fetch = FetchType.EAGER)
  @JoinTable(
      name = "appointment_activity",
      joinColumns = {@JoinColumn(name = "appointment_id")},
      inverseJoinColumns = {@JoinColumn(name = "activity_id")})
  private Set<Activity> activities = new HashSet();

  @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REMOVE,CascadeType.REFRESH,CascadeType.DETACH,CascadeType.PERSIST}
          ,mappedBy = "appointment")
  private AppointmentReminder reminder;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Set<Activity> getActivities() {
    return activities;
  }

  public void setActivities(Set<Activity> activities) {
    this.activities = activities;
  }

  public void calculateEndDate() {

    long activityLengthInMil = startDate.getTime();

    for (Activity activity : activities) {
      activityLengthInMil = activityLengthInMil + activity.getDuration() * 60_000;
    }

    endDate = new Date(activityLengthInMil);
  }

  public AppointmentReminder getReminder() {
    return reminder;
  }

  public void setReminder(AppointmentReminder reminder) {
    this.reminder = reminder;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Appointment that = (Appointment) o;
    return id == that.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
