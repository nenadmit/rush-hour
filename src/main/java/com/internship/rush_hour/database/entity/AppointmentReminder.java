package com.internship.rush_hour.database.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="appointments_reminder")
public class AppointmentReminder {

    @Id
    private int id;
    private Date reminderDate;

    @OneToOne(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name="appointment_id")
    private Appointment appointment;

    public Date getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(Date reminderDate) {
        this.reminderDate = reminderDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }
}
