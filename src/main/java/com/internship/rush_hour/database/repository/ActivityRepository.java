package com.internship.rush_hour.database.repository;

import com.internship.rush_hour.database.entity.Activity;
import com.internship.rush_hour.database.entity.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ActivityRepository extends JpaRepository<Activity,Integer> {

    Page<Activity> findAll(Pageable pageable);
    Page<Activity> findAllByAppointments(Appointment appointment, Pageable pageable);
}
