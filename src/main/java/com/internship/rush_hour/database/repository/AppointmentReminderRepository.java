package com.internship.rush_hour.database.repository;
import com.internship.rush_hour.database.entity.AppointmentReminder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface AppointmentReminderRepository extends JpaRepository<AppointmentReminder,Integer> {

    @Query("SELECT a FROM AppointmentReminder a WHERE a.reminderDate >=?1 AND a.reminderDate <= ?2")
    List<AppointmentReminder> findAllBetweenTwoDates(Date startDate, Date endDate);
}
