package com.internship.rush_hour.database.repository;

import com.internship.rush_hour.database.entity.Activity;
import com.internship.rush_hour.database.entity.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {

  Page<Appointment> findAll(Pageable pageable);

  Page<Appointment> findAllByStartDateAfter(Date date, Pageable pageable);

  Page<Appointment> findAllByStartDateBefore(Date date, Pageable pageable);

  Page<Appointment> findAllByUserId(int id, Pageable pageable);

  Optional<Appointment> findByIdAndUserId(int id, int userId);

  Page<Appointment> findAllByUserIdAndStartDateAfter(int userId, Date date, Pageable pageable);

  Page<Appointment> findAllByUserIdAndStartDateBefore(int userId, Date date, Pageable pageable);

  Page<Appointment> findAllByActivities(Activity activity, Pageable pageable);
  Set<Appointment> findAllByActivities(Activity activity);
  @Query(
      "SELECT a FROM Appointment a WHERE a.startDate <= ?1 AND a.endDate >= ?1 OR a.startDate <= ?2 AND a.endDate >= ?2")
  List<Appointment> findOverlappingAppointments(Date startDate, Date endDate);

}