package com.internship.rush_hour.exception_handler.exception;

public class EntityAlreadyPresentException extends RuntimeException {

    public EntityAlreadyPresentException(String message) {
        super(message);
    }
}
