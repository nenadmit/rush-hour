package com.internship.rush_hour.exception_handler.exception;

public class ParameterValidationException extends RuntimeException {

    public ParameterValidationException(String message) {
        super(message);
    }
}
