package com.internship.rush_hour.exception_handler;

import com.internship.rush_hour.exception_handler.exception.*;
import com.internship.rush_hour.exception_handler.pojo.ExceptionResponseEntity;
import com.internship.rush_hour.exception_handler.pojo.BindingResultErrorResponseEntity;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {

    BindingResultErrorResponseEntity error = new BindingResultErrorResponseEntity();
    error.setStatus(HttpStatus.BAD_REQUEST.value());
    error.setErrors(getErrorFieldsAsMap(ex.getBindingResult().getFieldErrors()));
    error.setTimeStamp(System.currentTimeMillis());

    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ParameterValidationException.class)
  public ResponseEntity<ExceptionResponseEntity> handleValidationException(
      ParameterValidationException ex) {

    ExceptionResponseEntity response =
        new ExceptionResponseEntity(
            HttpStatus.BAD_REQUEST.value(), ex.getMessage(), System.currentTimeMillis());
    response.setError(ex.getClass().getSimpleName());

    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(PropertyReferenceException.class)
  public ResponseEntity<ExceptionResponseEntity> handleValidationException(
      PropertyReferenceException ex) {

    ExceptionResponseEntity response =
        new ExceptionResponseEntity(
            HttpStatus.BAD_REQUEST.value(), ex.getMessage(), System.currentTimeMillis());
    response.setError(ex.getClass().getSimpleName());

    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(EntityAlreadyPresentException.class)
  public ResponseEntity<ExceptionResponseEntity> handleValidationException(
      EntityAlreadyPresentException ex) {

    ExceptionResponseEntity response =
        new ExceptionResponseEntity(
            HttpStatus.CONFLICT.value(), ex.getMessage(), System.currentTimeMillis());
    response.setError(ex.getClass().getSimpleName());

    return new ResponseEntity<>(response, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ExceptionResponseEntity> handleNotFoundException(
      EntityNotFoundException ex) {

    ExceptionResponseEntity response =
        new ExceptionResponseEntity(
            HttpStatus.NOT_FOUND.value(), ex.getMessage(), System.currentTimeMillis());
    response.setError(ex.getClass().getSimpleName());

    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
  }

  private Map<String, String> getErrorFieldsAsMap(List<FieldError> fieldErrors) {

    Map<String, String> errorMap = new LinkedHashMap<>();
    fieldErrors.forEach(
        fieldError -> errorMap.put(fieldError.getField(), fieldError.getDefaultMessage()));

    return errorMap;
  }
}
