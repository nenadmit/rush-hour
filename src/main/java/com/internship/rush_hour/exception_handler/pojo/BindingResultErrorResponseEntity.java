package com.internship.rush_hour.exception_handler.pojo;

import java.util.Map;

public class BindingResultErrorResponseEntity {

    private int status;
    private Map<String,String> errors;
    private long timeStamp;

    public BindingResultErrorResponseEntity() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
