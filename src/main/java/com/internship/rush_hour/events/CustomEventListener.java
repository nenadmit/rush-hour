package com.internship.rush_hour.events;

import com.internship.rush_hour.database.dto.UserDTO;
import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.events.mail.EventMailService;
import com.internship.rush_hour.events.mail_events.AppointmentReminderEvent;
import com.internship.rush_hour.events.mail_events.AppointmentScheduledEvent;
import com.internship.rush_hour.events.mail_events.SuccessfulRegistrationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class  CustomEventListener {

  private EventMailService mailService;

  @Autowired
  public CustomEventListener(EventMailService mailService) {
    this.mailService = mailService;
  }

  @EventListener(SuccessfulRegistrationEvent.class)
  @Async
  public void handleEvent(SuccessfulRegistrationEvent event) {


    mailService.sendSuccessfulRegistration(event.getEmail(), event.getName());
  }

  @EventListener(AppointmentScheduledEvent.class)
  @Async
  public void handleEvent(AppointmentScheduledEvent event) {

    Appointment appointment = event.getAppointment();
    mailService.sendAppointmentScheduled(
        appointment.getUser().getEmail(), appointment.getStartDate());
  }

  @EventListener(AppointmentReminderEvent.class)
  @Async
  public void handleEvent(AppointmentReminderEvent event) {

    Appointment appointment = event.getAppointment();
    mailService.sendAppointmentReminder(
        appointment.getUser().getEmail(), appointment.getStartDate());
  }
}
