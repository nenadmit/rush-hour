package com.internship.rush_hour.events.mail_events;

import org.springframework.context.ApplicationEvent;

public class SuccessfulRegistrationEvent extends ApplicationEvent {

    String email;
    String Name;

    public SuccessfulRegistrationEvent(Object source, String email, String name) {
        super(source);
        this.email = email;
        Name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
