package com.internship.rush_hour.events.mail_events;

import com.internship.rush_hour.database.entity.Appointment;
import org.springframework.context.ApplicationEvent;



public class AppointmentScheduledEvent extends ApplicationEvent {

    private Appointment appointment;

    public AppointmentScheduledEvent(Object source, Appointment appointment) {
        super(source);
        this.appointment = appointment;
    }

    public Appointment getAppointment() {
        return appointment;
    }
}


