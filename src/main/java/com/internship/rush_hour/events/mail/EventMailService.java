package com.internship.rush_hour.events.mail;

import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.database.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class EventMailService {

  private JavaMailSender mailSender;

  @Autowired
  public EventMailService(JavaMailSender mailSender) {
    this.mailSender = mailSender;
  }

  @Async
  public void sendSuccessfulRegistration(String email, String name) {

    SimpleMailMessage mailMessage = getMailMessage(email, "Successful Registration!");

    StringBuilder builder = new StringBuilder();
    builder.append(String.format("Hello, %s, \n", name));
    builder.append(
        "This is an email to remind you that you have successfully registered on our website.");
    mailMessage.setText(builder.toString());
    mailSender.send(mailMessage);
  }

  @Async
  public void sendAppointmentScheduled(String email, Date startDate) {

    SimpleMailMessage mailMessage = getMailMessage(email, "Appointment Scheduled!");
    mailMessage.setText(
        String.format(
            "You have successfully scheduled an appointment starting from: %s", startDate));
    mailSender.send(mailMessage);
  }

  @Async
  public void sendAppointmentReminder(String email, Date startDate) {

    SimpleMailMessage mailMessage = getMailMessage(email, "Appointment reminder for today!");
    mailMessage.setText(
        String.format(
            "This is a reminder that you appointment for date %s, will start in 30 minutes.",
            startDate));
    mailSender.send(mailMessage);
  }

  private SimpleMailMessage getMailMessage(String email, String title) {

    SimpleMailMessage mailMessage = new SimpleMailMessage();
    mailMessage.setFrom("info@dentist.com");
    mailMessage.setTo(email);
    mailMessage.setSubject(title);

    return mailMessage;
  }
}
