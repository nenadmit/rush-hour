package com.internship.rush_hour.events;

import com.internship.rush_hour.database.entity.AppointmentReminder;
import com.internship.rush_hour.database.service.ReminderService;
import com.internship.rush_hour.events.mail_events.AppointmentReminderEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class AppointmentReminderScheduler {

  private ApplicationEventPublisher eventPublisher;
  private ReminderService reminderService;
  private static final long MINUTE_IN_MILLISECONDS = 60_000;

  @Autowired
  public AppointmentReminderScheduler(
      ApplicationEventPublisher eventPublisher, ReminderService reminderService) {
    this.eventPublisher = eventPublisher;
    this.reminderService = reminderService;
  }

  @Scheduled(fixedRate = MINUTE_IN_MILLISECONDS * 5)
  @Async
  public void checkForUpcomingAppointments() {

    Date startDate = new Date();
    Date endDate = new Date(startDate.getTime() + MINUTE_IN_MILLISECONDS * 5);

    List<AppointmentReminder> reminders =
        reminderService.findAllBetweenTwoDates(startDate, endDate);

    if (!reminders.isEmpty()) {
      for (AppointmentReminder reminder : reminders) {
        eventPublisher.publishEvent(new AppointmentReminderEvent(this, reminder.getAppointment()));
        reminderService.deleteReminder(reminder);
      }
    }
  }
}
