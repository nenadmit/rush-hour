package com.internship.rush_hour.security;

import com.internship.rush_hour.security.filter.JwtValidationFilter;
import com.internship.rush_hour.security.user_details.CustomUserDetailsService;
import com.internship.rush_hour.security.filter.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.internship.rush_hour.security.SecurityConstants.*;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private CustomUserDetailsService customUserDetailsService;
  private JwtAuthenticationFilter jwtAuthenticationFilter;
  @Autowired
  private JwtValidationFilter jwtValidationFilter;
  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  public SecurityConfiguration(
      CustomUserDetailsService customUserDetailsService,
      JwtAuthenticationFilter jwtAuthenticationFilter) {
    this.customUserDetailsService = customUserDetailsService;
    this.jwtAuthenticationFilter = jwtAuthenticationFilter;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder);
  }

  @Override
  public void configure(WebSecurity web){
    web.ignoring().antMatchers(WHITELISTED_URL);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        .authorizeRequests()
         .antMatchers(HttpMethod.GET,ACTIVITIES_URL).hasRole(ROLE_USER)
        .antMatchers(ACTIVITIES_URL).hasRole(ROLE_ADMIN)
        .antMatchers(ADMIN_USER__URL).hasRole(ROLE_ADMIN)
        .antMatchers(ADMIN_APPOINTMENTS_URL).hasRole(ROLE_ADMIN)
        .antMatchers(USER_API_URL).hasAnyRole(ROLE_USER, ROLE_ADMIN)
        .anyRequest().authenticated()
        .and()
        .exceptionHandling()
        .and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.addFilterAfter(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    http.addFilterBefore(jwtValidationFilter,JwtAuthenticationFilter.class);
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
}
