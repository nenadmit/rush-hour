package com.internship.rush_hour.security;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SecurityConstants {

  /** Jwt constants */
  public static final String AUTH_HEADER = "Authorization";

  public static final String SECRET = "sS4flkF@#$sd#lkfa!34@fl%@aDASd32fd2wsd4#@4";
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final long EXPIRATION_TIME = System.currentTimeMillis() + 1000 * 60 * 60 * 10;

  /** Roles */
  public static final String ROLE_PREFIX = "ROLE_";

  public static final String ROLE_USER = "USER";
  public static final String ROLE_ADMIN = "ADMIN";

  /** Application URLS */
  private static final String PREFIX = "/api/";
  private static final String VERSION = "v1";
  public static final String LOGIN_URL = "/login";
  public static final String REGISTRATION_URL = "/register";
  public static final String ADMIN_USER__URL = "/api/users/**";
  public static final String ADMIN_APPOINTMENTS_URL = "/api/appointments/**";
  public static final String USER_API_URL = "/api/user/**";
  public static final String ACTIVITIES_URL = "/api/activities";
  protected static final String[] WHITELISTED_URL = new String[]{
          "/swagger-resources/**",
          "/swagger-ui.html/**",
          "/v2/api-docs",
          "/webjars/**",
          REGISTRATION_URL,
          LOGIN_URL
  };


  private SecurityConstants() {}


  public static List<String> getWhitelistedUrl(){

    return Arrays.stream(WHITELISTED_URL).collect(Collectors.toList());

  }
}
