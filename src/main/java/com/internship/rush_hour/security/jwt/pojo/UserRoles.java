package com.internship.rush_hour.security.jwt.pojo;

import com.internship.rush_hour.database.entity.Role;
import org.springframework.security.core.GrantedAuthority;

public class UserRoles implements GrantedAuthority {

    private Role role;

    public UserRoles(Role role) {
        this.role = role;
    }

    public UserRoles() {
    }

    @Override
    public String getAuthority() {
        return role.getName();
    }

}
