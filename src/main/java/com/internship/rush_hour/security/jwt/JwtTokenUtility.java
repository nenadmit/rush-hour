package com.internship.rush_hour.security.jwt;

import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.exception_handler.exception.JwtValidationException;
import com.internship.rush_hour.security.SecurityConstants;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class JwtTokenUtility {


  public String createToken(User user) {

    Map<String, Object> claims = new LinkedHashMap<>();
    claims.put("id", user.getId());
    claims.put("username", user.getEmail());

    return Jwts.builder()
        .setClaims(claims)
        .setSubject(user.getFirstName())
        .setIssuedAt(new Date())
        .setExpiration(new Date(SecurityConstants.EXPIRATION_TIME))
        .signWith(SignatureAlgorithm.HS256, SecurityConstants.SECRET)
        .compact();
  }

  public void validateToken(String token) {
    try {
      parseToken(token);
    } catch (SignatureException | MalformedJwtException e) {
      throw new JwtValidationException("Token signature invalid or malformed");
    }catch (ExpiredJwtException e){
      throw new JwtValidationException("Token has expired");
    }
  }

  public boolean isTokenNonExpired(String token) {
    return !getExpirationDate(token).before(new Date());
  }

  private Claims parseToken(String token) {
    return Jwts.parser().setSigningKey(SecurityConstants.SECRET).parseClaimsJws(token).getBody();
  }

  public String getUsername(String token) {
    return (String) parseToken(token).get("username");
  }

  private Date getExpirationDate(String token) {
    return parseToken(token).getExpiration();
  }
}
