package com.internship.rush_hour.security.filter;

import com.internship.rush_hour.exception_handler.exception.JwtValidationException;
import com.internship.rush_hour.security.SecurityConstants;
import com.internship.rush_hour.security.jwt.JwtTokenUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class JwtValidationFilter extends OncePerRequestFilter {

  private JwtErrorHandler errorHandler;
  private JwtTokenUtility jwtUtility;

  @Autowired
  public JwtValidationFilter(JwtErrorHandler errorHandler, JwtTokenUtility jwtUtility) {
    this.errorHandler = errorHandler;
    this.jwtUtility = jwtUtility;
  }

  /**
     * Checks if the authorization is present and contains a token with Bearer prefix
     * if token is present, check whether it can be validated.
     */


  @Override
  protected void doFilterInternal(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      FilterChain filterChain)
      throws ServletException, IOException {

    String header = httpServletRequest.getHeader(SecurityConstants.AUTH_HEADER);
    if (header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
        errorHandler.sendErrorResponse(
          new JwtValidationException("Authorization header doesn't contain a token with Bearer prefix"), httpServletResponse);
      return;
    }

    String token = header.substring(7);
    try {
      jwtUtility.validateToken(token);
      jwtUtility.isTokenNonExpired(token);
    } catch (JwtValidationException e) {
      errorHandler.sendErrorResponse(e, httpServletResponse);
      return;
    }

    filterChain.doFilter(httpServletRequest, httpServletResponse);
  }

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {


    AntPathMatcher pathMatcher = new AntPathMatcher();

    return SecurityConstants.getWhitelistedUrl().stream()
            .anyMatch(p -> pathMatcher.match(p, request.getServletPath()));
  }
}
