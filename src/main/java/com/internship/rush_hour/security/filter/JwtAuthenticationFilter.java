package com.internship.rush_hour.security.filter;

import com.internship.rush_hour.security.user_details.CustomUserDetailsService;
import com.internship.rush_hour.security.jwt.JwtTokenUtility;
import com.internship.rush_hour.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Service
public class JwtAuthenticationFilter extends OncePerRequestFilter {

  private CustomUserDetailsService userDetailsService;
  private JwtTokenUtility jwtUtility;
  private JwtErrorHandler errorHandler;

  @Autowired
  public JwtAuthenticationFilter(
      CustomUserDetailsService userDetailsService,
      JwtTokenUtility jwtUtility,
      JwtErrorHandler errorHandler) {
    this.userDetailsService = userDetailsService;
    this.jwtUtility = jwtUtility;
    this.errorHandler = errorHandler;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      FilterChain filterChain)
      throws ServletException, IOException {

    String token = httpServletRequest.getHeader(SecurityConstants.AUTH_HEADER).substring(7);
    String username = jwtUtility.getUsername(token);
    UserDetails userDetail;
    try{
      userDetail = userDetailsService.loadUserByUsername(username);
    }catch (UsernameNotFoundException e){
      errorHandler.sendErrorResponse(
              e, httpServletResponse);
      return;
    }

    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
        new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities());
    usernamePasswordAuthenticationToken.setDetails(
        new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

    filterChain.doFilter(httpServletRequest, httpServletResponse);
  }

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {

    AntPathMatcher pathMatcher = new AntPathMatcher();

    return SecurityConstants.getWhitelistedUrl().stream()
        .anyMatch(p -> pathMatcher.match(p, request.getServletPath()));
  }

}
