package com.internship.rush_hour.security.user_details;

import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Optional<User> userOptional = userService.findByEmail(s);
        CustomUserDetails userDetails = new CustomUserDetails();


        if(userOptional.isPresent()){
            userDetails.setUser(userOptional.get());
        }else{
            throw new UsernameNotFoundException("User not found");
        }

        return userDetails;
    }
}
