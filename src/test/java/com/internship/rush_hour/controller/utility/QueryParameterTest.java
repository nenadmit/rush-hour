package com.internship.rush_hour.controller.utility;

import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.database.EntityProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class QueryParameterTest {


    private EntityProvider entityProvider = new EntityProvider();

    @DisplayName("Does Page return the same number of elements using QueryParameter")
    @Test
    public void testPageable(){

        QueryParameter parameter = new QueryParameter();
        parameter.setSize(3);
        parameter.setPage(1);

        List<Appointment> appointmentList = entityProvider.getAllAppointments();

        Page<Appointment> testPage = new PageImpl<Appointment>(appointmentList,parameter.getPageable(),appointmentList.size());

        Assertions.assertEquals(parameter.getSize(),testPage.getSize());

    }

}