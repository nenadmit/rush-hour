package com.internship.rush_hour.database.service;

import com.internship.rush_hour.database.EntityProvider;
import com.internship.rush_hour.database.dto.AppointmentDTO;
import com.internship.rush_hour.database.dto.UpdateAppointmentDTO;
import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.repository.AppointmentRepository;
import com.internship.rush_hour.exception_handler.exception.EntityAlreadyPresentException;
import com.internship.rush_hour.exception_handler.exception.EntityNotFoundException;
import com.internship.rush_hour.exception_handler.exception.ParameterValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AppointmentServiceTest {

  private EntityProvider entityProvider = new EntityProvider();
  private ModelMapper modelMapper = new ModelMapper();

  @Mock private AppointmentRepository appointmentRepository;

  @InjectMocks private AppointmentService appointmentService;

  @Test
  @DisplayName(
      "Tests for date overlap, throws an exception if an appointment is already present in database")
  public void testForDateOverlap() {

    Appointment appointment = entityProvider.createAppointment("2020-10-10 15:00");
    List<Appointment> appointmentList =
        Arrays.asList(entityProvider.createAppointment("2020-10-10 14:00"));

    when(appointmentRepository.findOverlappingAppointments(
            appointment.getStartDate(), appointment.getEndDate()))
        .thenReturn(appointmentList);

    assertThrows(RuntimeException.class, () -> appointmentService.checkForDateOverlap(appointment));

    when(appointmentRepository.findOverlappingAppointments(
            appointment.getStartDate(), appointment.getEndDate()))
        .thenReturn(Arrays.asList(appointment));

    assertDoesNotThrow(() -> appointmentService.checkForDateOverlap(appointment));
  }

  @Test
  @DisplayName("Is appointment in valid state before persisting")
  public void checkPrePersist() {

    Appointment appointment = entityProvider.createAppointment("2020-05-05 14:00");

    appointment = appointmentService.prepareForPersist(appointment, new User());
    assertNotNull(appointment.getUser());
    assertNotNull(appointment.getEndDate());
    assertNotNull(appointment.getReminder());
  }

  @Test
  @DisplayName("Throws NotFoundException if repository returns Optional<Empty>")
  public void findById() {

    Appointment appointment = entityProvider.createAppointment("2020-10-14 14:00");
    when(appointmentRepository.findById(appointment.getId())).thenReturn(Optional.empty());

    assertThrows(EntityNotFoundException.class,()->appointmentService.findById(appointment.getId()));

    when(appointmentRepository.findById(appointment.getId())).thenReturn(Optional.of(appointment));

    assertDoesNotThrow(()->appointmentService.findById(appointment.getId()));
  }

  @Test
  public void findByIdAndUserId(){
    Appointment appointment = entityProvider.createAppointment("2020-10-14 14:00");
    when(appointmentRepository.findByIdAndUserId(appointment.getId(),1)).thenReturn(Optional.empty());

    assertThrows(EntityNotFoundException.class,()->appointmentService.findByIdAndUserId(appointment.getId(),1));

  }

  @Test
  public void deleteAppointment(){
    Appointment appointment = entityProvider.createAppointment("2020-10-14 14:00");
    when(appointmentRepository.findById(appointment.getId())).thenReturn(Optional.of(appointment));
    assertDoesNotThrow(()->appointmentService.delete(appointment.getId()));
  }


}
