package com.internship.rush_hour.database.service;

import com.internship.rush_hour.database.EntityProvider;
import com.internship.rush_hour.database.entity.User;
import com.internship.rush_hour.database.repository.UserRepository;
import com.internship.rush_hour.exception_handler.exception.EntityAlreadyPresentException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

  private EntityProvider entityProvider = new EntityProvider();
  private ModelMapper modelMapper = new ModelMapper();

  @Mock private UserRepository userRepository;

  @InjectMocks private UserService userService;

  @DisplayName("Check if user is in valid state before persisting")
  @Test
  public void prepareForPersist() {}
}
