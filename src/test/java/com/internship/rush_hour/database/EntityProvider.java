package com.internship.rush_hour.database;

import com.internship.rush_hour.database.dto.ActivityDTO;
import com.internship.rush_hour.database.dto.AppointmentDTO;
import com.internship.rush_hour.database.entity.Activity;
import com.internship.rush_hour.database.entity.Appointment;
import com.internship.rush_hour.database.entity.Role;
import com.internship.rush_hour.database.entity.User;
import org.modelmapper.ModelMapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
public class EntityProvider {

  private List<Appointment> allAppointments = new ArrayList<>();
  private  ModelMapper mapper = new ModelMapper();

  public  User getUser() {

    User user = new User();
    user.setId(10);
    user.setEmail("nenadmit@outlook.com");
    user.setPassword("dreadw4r");
    user.setFirstName("Nenad");
    user.setLastName("Mitkovic");

    user.getRoles().add(new Role("ROLE_ADMIN"));

    return user;
  }

  public Set<Activity> getActivities() {
    Set<Activity> activities = new HashSet();
    activities.add(new Activity("dental", 10, 29.99));
    activities.add(new Activity("activity2", 15, 39.99));
    activities.add(new Activity("activity3", 20, 49.99));

    return activities;
  }

  public  Set<Activity> getActivitiesAsSet(){
    return getActivities().stream().collect(Collectors.toSet());
  }

  public Appointment createAppointment(String startDate) {

    Appointment appointment = new Appointment();
    appointment.setId(new Random().nextInt(50) + 1);
    Date date = new Date();
    try {
      date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(startDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    appointment.setStartDate(date);
    appointment.setActivities(getActivities());
    appointment.calculateEndDate();

    return appointment;
  }

  public  List<Appointment> getAllAppointments() {

      if(allAppointments.isEmpty()){
          setAllAppointments();
      }

    return allAppointments;
  }

  public void setAllAppointments() {
    allAppointments =
        Arrays.asList(
            createAppointment("2020-05-05 15:00"),
            createAppointment("2020-05-05 15:00"),
            createAppointment("2020-05-05 15:00"),
            createAppointment("2020-05-05 15:00"),
            createAppointment("2020-05-05 15:00"),
            createAppointment("2020-05-05 15:00"));
  }

  public Set<Appointment> getAllApointmentsAsSet(){
    return getAllAppointments().stream().collect(Collectors.toSet());
  }

  public AppointmentDTO getDTO(Appointment appointment){
    ModelMapper modelMapper = new ModelMapper();
    return mapper.map(appointment, AppointmentDTO.class);
  }

  public Appointment getEntity(AppointmentDTO appointmentDTO){
    return mapper.map(appointmentDTO,Appointment.class);
  }

  public ActivityDTO getDTO(Activity activity){
    return mapper.map(activity, ActivityDTO.class);
  }
}
