package com.internship.rush_hour.database.entity;

import com.internship.rush_hour.database.EntityProvider;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AppointmentTest {

  private EntityProvider entityProvider = new EntityProvider();

  @DisplayName("Is end date calculated correctly based on list of activities")
  @Test
  void calculateEndDate() {

    Appointment appointment = new Appointment();
    appointment.setStartDate(new Date());
    appointment.setActivities(entityProvider.getActivities());
    appointment.calculateEndDate();

    assertNotNull(appointment.getEndDate());
    long timeMilis = appointment.getStartDate().getTime();
    for(Activity a: appointment.getActivities()){
      timeMilis = timeMilis + a.getDuration() * 60_000;
    }
    Date endDate = new Date(timeMilis);
    assertEquals(appointment.getEndDate(),endDate);


  }
}