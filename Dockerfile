FROM openjdk:8
ADD target/rush-hour.jar rush-hour.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","rush-hour.jar"]
